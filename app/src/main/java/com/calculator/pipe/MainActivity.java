package com.calculator.pipe;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText inputField;
    TextView output;
    TextView outputTitle;
    Handler mHandler;

    StringBuilder log = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();

        output = findViewById(R.id.output);
        outputTitle = findViewById(R.id.output_title);
        inputField = findViewById(R.id.et_pipe_length);
        inputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                output.setText("");
                inputField.setError(null);

                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute(inputField.getText().toString());
            }
        });


    }


    public void log(String line) {

        log.append(line);
        log.append("\n");



    }

    public void calculate(String len) {
        log = new StringBuilder();


        Integer length = null;
        if(!TextUtils.isEmpty(len)) {
            try {
                length = Integer.valueOf(len);
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inputField.setError("Please enter a valid length");
                    }
                });

            }
        } else {
            length = 0;
        }

        log("Pipe length [" + length + "] mm.");

        Integer lessError = 1000000;
        Integer lessErrorDistance = 100000;

        for (int distance = 550; distance <= 750; distance++) {
            Integer er = errorCalc(distance, length);
            if (er < lessError) {
                lessError = er;
                lessErrorDistance = distance;
            }
        }

        log("Minimal Error - > [" + lessError + "] mm");
        log("Mounting Distance at Minimal Error- > [" + lessErrorDistance + "] mm");


        Integer error = errorCalc(lessErrorDistance, length);


        log("---------------------------------------");
        log("Left  mounting at [100]mm");

        Integer cumulitativeDistance = 200;


        while ((cumulitativeDistance + lessErrorDistance) < length) {

            cumulitativeDistance += lessErrorDistance;
            log("Next  mounting at [" + (cumulitativeDistance - 100) + "]mm");
        }
        log("Right mounting at ["+(length-100)+"]mm");

        log("---------------------------------------");


    }


    static Integer errorCalc(Integer delta, Integer lengthOfPipe) {


        Integer maxMountedLength = lengthOfPipe - 200;
        Integer distanceBetweenMounting = delta;

        //System.out.println("Left mounting at [100]mm");

        Integer mountedLength = 0;

        while (maxMountedLength >= (mountedLength+distanceBetweenMounting)) {
            mountedLength+=distanceBetweenMounting;
            //  System.out.println("mounting at ["+(mountedLength+100)+"]");
        }
        Integer error = maxMountedLength - mountedLength;

        return  error;
    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Please Wait..."); // Calls onProgressUpdate()
            try {
                calculate(params[0]);
                resp = log.toString();


            }  catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            output.setText(result);
            outputTitle.setVisibility(inputField.getText().toString().length() > 0 ? View.VISIBLE: View.INVISIBLE);
        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this,
                    "Please Wait",
                    "Calculating...");
        }


        @Override
        protected void onProgressUpdate(String... text) {
            output.setText(text[0]);

        }
    }

}
